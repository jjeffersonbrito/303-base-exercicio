#Alpine image with OpenJDK JRE 8
FROM openjdk:8-alpine

COPY target/investimentos.jar /home/java/apps/investimentos.jar

# Para usar os argumentos (credenciais do mysql) que foram passados pelo Jenkins como argumentos
# do comando docker.build, precisamos receber esse argumentos dentro do Dockerfile definindo
# através do comando ARG qual o nome de cada argumento passado.
ARG mysql_host
ARG mysql_port
ARG mysql_user
ARG mysql_pass

# Com o bloco comando ENV, definimos que os valores dos argumentos passados no comando docker.build e
# recebidos pelos comandos ARG, serão utilizados para definir variáveis de ambiente que serão acessíveis
# em tempo de execução do container (após do docker run). Por exemplo, dizemos que dentro do container
# existirá uma variável de ambiente chamada mysql_host que armazena o valor passado por argumento através
# do arg $mysql_host.
ENV mysql_host=$mysql_host mysql_port=$mysql_port mysql_user=$mysql_user mysql_pass=$mysql_pass

# Container workdir
WORKDIR /home/java/apps

#Start
CMD ["java", "-jar", "-DskipTests", "investimentos.jar"]